import { SalaxyTrainingNgEx2Page } from './app.po';

describe('salaxy-training-ng-ex2 App', () => {
  let page: SalaxyTrainingNgEx2Page;

  beforeEach(() => {
    page = new SalaxyTrainingNgEx2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
