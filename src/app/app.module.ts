import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { Routes, RouterModule } from '@angular/router';
// import 'hammerjs';

import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';

import {  CalculatorComponent } from '@salaxy/ng/components'
import {  TokenComponent } from '@salaxy/ng/components'
//import { SalaxyComponentsModule, CalculatorComponent } from '@salaxy/ng/components'
import { SalaxyComponentsModule } from '@salaxy/ng/components'
import { SalaxyServicesModule, SessionService, SalaxyContext } from '@salaxy/ng/services';
import { API_BASE_URL } from '@salaxy/ng/core';

export const routes: Routes = [
  { path: '', component: WelcomeComponent },
  { path: 'Calculator', component: CalculatorComponent },
  { path: 'access_token/:accessToken', component: TokenComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule.forRoot(),
    RouterModule.forRoot(routes),
    SalaxyComponentsModule, 
    SalaxyServicesModule
  ],
  providers: [{ provide: API_BASE_URL, useValue: "https://test-api.salaxy.com" }, SessionService, SalaxyContext],
  bootstrap: [AppComponent]
})
export class AppModule { }
